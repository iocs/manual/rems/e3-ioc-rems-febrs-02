require essioc
require crome

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet(IOCNAME, "$(IOCNAME)")

epicsEnvSet(DEVICE_HOSTNAME, "rems-amm-1002.tn.esss.lu.se")
epicsEnvSet(P, "REMS-FEBRS:")
epicsEnvSet(R, "RMT-AMM-002:")

iocshLoad("$(crome_DIR)/crome.iocsh", "DEVICE_HOSTNAME=$(DEVICE_HOSTNAME), P=$(P), R=$(R)")
